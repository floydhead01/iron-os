use std::env;
use std::fs;
use std::fs::File;
use std::io::Read;
use std::io::Write;
use std::mem::transmute;

struct Header {
    len: u32,
    file_disciptors: Vec<Discriptor>,
}

struct Discriptor {
    magic: u8,
    name: Vec<u8>,
    offset: u32,
    length: u32,
}

fn main() {
    // fetch arguments
    let args: Vec<String> = env::args().collect();

    if args.len() != 3 {
        println!("Missing arguments");
        return;
    }
    let ref dir = &args[1];
    let ref img_file = &args[2];

    let mut buffer = Vec::new();
    let mut header = Header { len: 0, file_disciptors: Vec::new()};


    println!("Source Folder: {:?}", dir);

    println!("Generating Image: {:?}", img_file);

    let paths = fs::read_dir(dir).unwrap();

    for path in paths {
        let fpath = path.unwrap();
        let filepath: String = fpath.path().clone().display().to_string();
        let filename: String = fpath.file_name().into_string().unwrap();
        let fs = filename.clone();
        println!("Processing : {}", filename);

        let mut f = File::open(filepath).expect("file not found");
        
        let mut pl = buffer.len();
 
        f.read_to_end(&mut buffer).expect("could not read file");;

        let mut post = buffer.len();
  

        let mut len: u32 = ((post - pl)) as u32;

        header.len = header.len + 1;
 
        header.file_disciptors.push( Discriptor {magic: 0xBF, name: fs.into_bytes(), offset: 0, length: len});
        

    }


    let header_size: u32 = (((64+1+4+4) * header.len) + 4) as u32;
    let mut preoffset: u32  = header_size;


    //let mut buffer_size = header_size / 8;
    
    println!("Writing..");

    let mut img = File::create(img_file).expect("file not found");

    let hlen: [u8; 4] = unsafe { transmute(header.len.to_be()) };
    img.write(&hlen).expect("could not write file");


    for mut discriptor in header.file_disciptors {
        discriptor.offset = preoffset;
        preoffset += discriptor.length;
        let m: Vec<u8> = discriptor.name.clone();
        let s = String::from_utf8(m);
        //buffer_size += discriptor.length / 8;
        println!("    {:?}: Magic:{:?}, Len: {:?}, Offset:{:?}", s, discriptor.magic, discriptor.length, discriptor.offset);

        let mut magic: [u8; 1] = [discriptor.magic];

        img.write(&magic).expect("could not write file");

        let mut fname: [u8; 64] = [0; 64];
        let mut i = 0;
        for x in 0..64 {
            fname[x] = 32;
        }
        for byte in discriptor.name {
            if i < 64 {
                fname[i] = byte;
                i = i + 1;
            }
        }
        img.write(&fname).expect("could not write file");
        let offset: [u8; 4] = unsafe { transmute(discriptor.offset.to_be()) };
        let length: [u8; 4] = unsafe { transmute(discriptor.length.to_be()) };
        img.write(&offset).expect("could not write file");
        img.write(&length).expect("could not write file");
        
    }
    let content_size = buffer.len() * 8;

    println!("   writting content: {} bytes", buffer.len());
    img.write(buffer.as_slice()).expect("could not write file");

    let full_size = (header_size * 8) + (content_size as u32);
    let align = (4096  - (full_size / 8) );
    println!("   file size: {}", full_size);
    println!("   page align: {}", align);
    let align_pad: Vec<u8> = vec![0; (align) as usize];
     println!("   writting pad");
    img.write(align_pad.as_slice()).expect("could not write file");

    println!("Image Written");
    
}
