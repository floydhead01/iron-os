# IRON OS
A hobby 64 bit operating system written in Rust.


## Dependencies 
This project requires the following toolchain to build

1. NASM (http://www.nasm.us/)
2. GCC Linker
3. Rust (see below for setup instructions)
4. Grub with xorriso and mtools (EFI) installed
5. QEMU for emulation (although any VM will work)

### Rust Setup
1. install rustup
2. configure to use nightly build
```
rustup override add nightly
```
3. install xargo
```
cargo install xargo
```
4. install rust source
```
rustup component add rust-src
```


## Compiling 
Make sure the RUST_TARGET_PATH is set to your current directory so xCargo can compile
```
export RUST_TARGET_PATH=$PWD
```

Once that is done you can compile with the below command

``` 
make iso
```

## Running 
To run simply execute a virtual machine with the CDROM mounted to the generate iso file. 

## Current Features
1. Virtual Memory / Page Tables
2. Full Interupt Support
3. Simple Read Only Filesystem
4. Coopeartive Multitasking 
