
pub fn rax() -> usize {
    let ret: u64;
    unsafe { asm!("movq %rax, $0" : "=r" (ret)) };
    ret as usize
}

pub fn set_rax(value: usize) {
    unsafe { asm!("movq $0, %rax" :: "r" (value)) };
}

pub fn rbx() -> usize {
    let ret: u64;
    unsafe { asm!("movq %rbx, $0" : "=r" (ret)) };
    ret as usize
}

pub fn set_rbx(value: usize) {
    unsafe { asm!("movq $0, %rbx" :: "r" (value)) };
}

pub fn rcx() -> usize {
    let ret: u64;
    unsafe { asm!("movq %rcx, $0" : "=r" (ret)) };
    ret as usize
}
pub fn set_rcx(value: usize) {
    unsafe { asm!("movq $0, %rcx" :: "r" (value)) };
}

pub fn rdx() -> usize {
    let ret: u64;
    unsafe { asm!("movq %rdx, $0" : "=r" (ret)) };
    ret as usize
}

pub fn set_rdx(value: usize) {
    unsafe { asm!("movq $0, %rdx" :: "r" (value)) };
}

pub fn rsi() -> usize {
    let ret: usize;
    unsafe { asm!("movq %rsi, $0" : "=r" (ret)) };
    ret as usize
}
pub fn set_rsi(value: usize) {
    unsafe { asm!("movq $0, %rsi" :: "r" (value)) };
}

pub fn rsp() -> usize {
    let ret: u64;
    unsafe { asm!("movq %rsp, $0" : "=r" (ret)) };
    ret as usize
}
pub fn rbp() -> usize {
    let ret: u64;
    unsafe { asm!("movq %rbp, $0" : "=r" (ret)) };
    ret as usize
}

pub fn set_rbp(value: usize) {
    unsafe { asm!("movq $0, %rbp" :: "r" (value)) };
}

pub fn set_rsp(value: usize) {
    unsafe { asm!("movq $0, %rsp" :: "r" (value)) };
}

pub fn rip() -> usize {
    let ret: u64;
    unsafe { 
        asm!(" lea (%rip), %rax
               mov %rax, $0" : "=r" (ret)) 
    };
    ret as usize
}

pub fn flags() -> usize {
    let r: usize;
    unsafe { asm!("pushfq; popq $0" : "=r"(r) :: "memory") };
    r 
}
pub fn set_flags(val: usize) {
    unsafe { asm!("pushq $0; popfq" :: "r"(val) : "memory" "flags") };
}
