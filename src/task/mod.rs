use x86_64::PhysicalAddress;
use x86_64::VirtualAddress;
use x86_64::structures::tss::TaskStateSegment;
use x86_64::registers::control_regs;
use alloc::Vec;


use memory::paging::{InactivePageTable};
use memory::MemoryController;
use interrupts;
use spin::Mutex;
mod registers;

lazy_static! {
    pub static ref SCH:Mutex<Schedular> = Mutex::new(Schedular {
        tasks: vec![],
        current_task: 0
    });
}

#[derive(Clone)]
pub struct Task {
    pub rsp: usize,
    pub rbp: usize,
    pub rip: usize,
    pub cr3: usize,
    pub new: bool,
}

pub struct Schedular {
    pub tasks: Vec<Task>,
    pub current_task: usize,
}

impl Schedular {
    pub fn init(&mut self, memory_controller: &mut MemoryController){
        self.current_task = 0;

        //make dummy kernel task. No need to set instruction point cause it never enters cleanly
        let kernel_task: Task = Task {
            rsp: 0,
            rbp: 0,
            new: false,
            rip: 0,
            cr3: 0,
        };

        self.tasks.push(kernel_task);
    }

    pub fn get_task(&mut self, id: usize) -> Option<&mut Task> {
        self.tasks.iter_mut().nth(id)
    }
    pub fn create_task(&mut self, entry_point: usize, eflags: usize, memory_controller: &mut MemoryController) {
        //make stack for new task :)
        let new_process_stack = memory_controller.alloc_stack(1).unwrap();
        //let rip_address = VirtualAddress(new_process_stack.top());
        //force TLB reset. Performance hit but want to make sure my memory is writable (NOTE MAY NOT NEED)
        use x86_64::instructions::tlb;
        tlb::flush_all();

        println!("Creating new task:");
        println!("    stack top: {:x}",new_process_stack.top());
        println!("    enter point address: {:x}",entry_point);
       

        let mut temp_page = memory_controller.active_table.clone_page_table(&mut memory_controller.frame_allocator);

        let new_task: Task = Task {
            rsp: new_process_stack.top(), 
            rbp: new_process_stack.top(),
            new: true,
            rip: entry_point,
            cr3: temp_page.p4_frame.start_address(),
        };

        self.tasks.push(new_task);
    }
    
}

//yeilds to a task
pub fn yeild() {
    let mut next_task = SCH.lock().current_task.clone() + 1;
    if(next_task == SCH.lock().tasks.len()) {
        next_task = 0;
    }
    switch_task(next_task);
}

//switches_task
pub fn switch_task( pid: usize) {

    println!("switching task {:x}",pid);

    unsafe {
        let next_rsp = SCH.lock().get_task(pid).unwrap().rsp.clone();
        let next_rbp = SCH.lock().get_task(pid).unwrap().rbp.clone();
        let next_rip = SCH.lock().get_task(pid).unwrap().rip.clone();
        let old_task = SCH.lock().current_task.clone();
        let new = SCH.lock().get_task(pid).unwrap().new.clone();

        //set task to no longer new
        if(new) {
            SCH.lock().get_task(pid).unwrap().new = false;
        }

        asm!("sti");
        let mut rsps: u64 = 0;
        let mut rbps: u64 =0;
        let mut rips: u64 =0;

        //read old RSP, RBP, RIP registers
        asm!("movq %rsp, $0;
                movq %rbp, $1;
                lea (%rip), %rax;
                mov %rax, $2" : "=r" (rsps), "=r"(rbps), "=r"(rips));

        //set old task registers
        SCH.lock().get_task(old_task).unwrap().rsp = rsps as usize;
        SCH.lock().get_task(old_task).unwrap().rbp = rbps as usize;
        SCH.lock().get_task(old_task).unwrap().rip = rips as usize;
            
        SCH.lock().current_task = pid;

        //check to make sure we have a stack to jump too...we better
        if(next_rsp != 0 ) {
            //set the stack pointer to the stack we want to jump too
            asm!("movq $0, %rsp":: "r"(next_rsp) :: "volatile");
        }

        //if new trick stack to jmp to entry point
        if( new == true ) {
            //else move to entry point
            asm!("pushq $0;
                retq" :: "r" (next_rip))
        }
    }
}

