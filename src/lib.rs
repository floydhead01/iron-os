#![feature(lang_items)]
#![feature(const_fn)]
#![feature(alloc)]
#![feature(const_unique_new, const_atomic_usize_new)]
#![feature(unique)]
#![feature(allocator_api)]
#![feature(global_allocator)]
#![feature(ptr_internals)]
#![feature(pointer_methods)]
#![feature(abi_x86_interrupt)]
#![feature(type_ascription)]
#![no_std]
#![feature(asm)]

#[macro_use]
extern crate alloc;
extern crate rlibc;
extern crate volatile;
extern crate spin;
extern crate multiboot2;
#[macro_use]
extern crate bitflags;
extern crate x86_64;
#[macro_use]
extern crate once;
#[macro_use]
extern crate lazy_static;
extern crate bit_field;

#[macro_use]
mod vga_buffer;
mod memory;
mod interrupts;
mod hardware;
mod drivers;
mod utils;
mod task;


#[no_mangle]
pub extern fn rust_main(multiboot_information_address: usize) {
    //use memory::FrameAllocator;

    vga_buffer::clear_screen();
    println!("Hello World{}", "!");

    vga_buffer::set_color( vga_buffer::Color::Red, vga_buffer::Color::Black );
    
    let boot_info = unsafe {
        multiboot2::load(multiboot_information_address)
    };

    let mut modules = boot_info.module_tags();

    enable_nxe_bit();
    enable_write_protect_bit();

    // set up guard page and map the heap pages
    let mut memory_controller = memory::init(&boot_info);

    unsafe {
        HEAP_ALLOCATOR.lock().init(HEAP_START, HEAP_START + HEAP_SIZE);
    }

    hardware::pic::init();
    
    //turn on keyboard
    hardware::pic::set_irq(1, true);

    interrupts::init(&mut memory_controller);

    hardware::pic::enable_hardware();

    println!("Setting up VFS");

    //sets up filesystem
    drivers::vfs::VFS.lock().init();

    match modules.next() {
        Some(module) => {
            let mut ramfs = drivers::ramfs::RamFS {};
            ramfs.init(module.start_address());
            //drivers::ramfs::RAMFS.lock().init(module.start_address()),
        }
        None => {}
    }
    
    use alloc::String;
    use drivers::vfs::node::{Node, DirEntry};
    use alloc::Vec;



    //TODO: Refactor to helper functions to prevent Lock Hell
    {
        let mut vfslock = drivers::vfs::VFS.lock();
        let root = vfslock.get_node(String::from("/initd")).unwrap();
        let root_info = root.get_info();

        println!("contents of {}", &*String::from_utf8(root_info.name.clone()).unwrap());

        let dirs: Vec<DirEntry>= root.read_dir();

        for dir in dirs {
            println!("    {}", &*String::from_utf8(dir.name).unwrap() );
        }
    }
    
    //TODO: Refactor to helper functions to prevent Lock Hell
    {
        let mut vfslock = drivers::vfs::VFS.lock();
        println!("Testing Read of File /initd/test.txt");
    
        let test_file = vfslock.get_node(String::from("/initd/test.txt")).unwrap();
        let mut contents: Vec<u8> = vec![];
        let test_file_info =  test_file.get_info();
        test_file.read(&0, &test_file_info.length, &mut contents);
    
        let str_contents: String = String::from_utf8(contents).unwrap();
        println!("Contents: {}", &*str_contents);
    }
    use task::SCH;


    let f = test_guy as usize;
   
    SCH.lock().init(&mut memory_controller);
    SCH.lock().create_task(f, 0, &mut memory_controller);
    task::yeild();

    println!("It did not crash!");
    task::yeild();
    loop{}
}

pub fn test_guy(){
    println!("Wow i made new thread");
    use task::SCH;
    task::yeild();
    println!("It did not crash!22");
    loop{}
    
}

#[lang = "eh_personality"] #[no_mangle] pub extern fn eh_personality() {}

#[lang = "panic_fmt"] 
#[no_mangle] 
pub extern fn panic_fmt(fmt: core::fmt::Arguments, file: &'static str,
    line: u32) -> !
{
    println!("\n\nPANIC in {} at line {}:", file, line);
    println!("    {}", fmt);
    loop{}
}

fn enable_nxe_bit() {
    use x86_64::registers::msr::{IA32_EFER, rdmsr, wrmsr};

    let nxe_bit = 1 << 11;
    unsafe {
        let efer = rdmsr(IA32_EFER);
        wrmsr(IA32_EFER, efer | nxe_bit);
    }
}
fn enable_write_protect_bit() {
    use x86_64::registers::control_regs::{cr0, cr0_write, Cr0};

    unsafe { cr0_write(cr0() | Cr0::WRITE_PROTECT) };
}

use memory::LockedHeap;

pub const HEAP_START: usize = 00_000_001_000_000_0000;
pub const HEAP_SIZE: usize = 100 * 1024; // 100 KiB

#[global_allocator]
static HEAP_ALLOCATOR: LockedHeap = LockedHeap::empty();

