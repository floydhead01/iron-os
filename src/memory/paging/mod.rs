pub use self::entry::*;
pub use self::mapper::Mapper;
//use self::table::{Table, Level4};
use self::temporary_page::TemporaryPage;
//use core::ptr::Unique;
use memory::{PAGE_SIZE, Frame, FrameAllocator};
use core::ops::{Deref, DerefMut};
use multiboot2::BootInformation;

mod entry;
mod temporary_page;
mod table;
mod mapper;

const ENTRY_COUNT: usize = 512;

pub type PhysicalAddress = usize;
pub type VirtualAddress = usize;

#[derive(Debug, Clone, Copy,PartialEq, Eq, PartialOrd, Ord)]
pub struct Page {
   number: usize,
}

impl Page {
    pub fn containing_address(address: VirtualAddress) -> Page {
        assert!(address < 0x0000_8000_0000_0000 ||
            address >= 0xffff_8000_0000_0000,
            "invalid address: 0x{:x}", address);
        Page { number: address / PAGE_SIZE }
    }
    pub fn range_inclusive(start: Page, end: Page) -> PageIter {
        PageIter {
            start: start,
            end: end,
        }
    }

    pub fn start_address(&self) -> usize {
        self.number * PAGE_SIZE
    }

    fn p4_index(&self) -> usize {
        (self.number >> 27) & 0o777
    }
    fn p3_index(&self) -> usize {
        (self.number >> 18) & 0o777
    }
    fn p2_index(&self) -> usize {
        (self.number >> 9) & 0o777
    }
    fn p1_index(&self) -> usize {
        (self.number >> 0) & 0o777
    }
}

#[derive(Clone)]
pub struct PageIter {
    start: Page,
    end: Page,
}

impl Iterator for PageIter {
    type Item = Page;

    fn next(&mut self) -> Option<Page> {
        if self.start <= self.end {
            let page = self.start;
            self.start.number += 1;
            Some(page)
        } else {
            None
        }
    }
}

#[derive(Clone)]
pub struct ActivePageTable {
    mapper: Mapper,
}

impl Deref for ActivePageTable {
    type Target = Mapper;

    fn deref(&self) -> &Mapper {
        &self.mapper
    }
}

impl DerefMut for ActivePageTable {
    fn deref_mut(&mut self) -> &mut Mapper {
        &mut self.mapper
    }
}

impl ActivePageTable {
    unsafe fn new() -> ActivePageTable {
        ActivePageTable {
            mapper: Mapper::new(),
        }
    }
    pub fn switch(&mut self, new_table: InactivePageTable) -> InactivePageTable {
        use x86_64::PhysicalAddress;
        use x86_64::registers::control_regs;

        let old_table = InactivePageTable {
            p4_frame: Frame::containing_address(
                control_regs::cr3().0 as usize
            ),
        };
        unsafe {
            control_regs::cr3_write(PhysicalAddress(
                new_table.p4_frame.start_address() as u64));
        }
        old_table
    }


    pub fn test_walk(&mut self) {

        println!("dumping page table");

        let p4 = self.mapper.p4_mut();
        for x in 0..511 {
            if( p4[x].is_unused() == false ) {
                println!("entry: {:#x}", p4[x].get());
                let p3 = p4.next_table_mut(x).unwrap();
                for y in 0..511 {
                    if(p3[y].is_unused() == false) {
                        println!("  entry: {:#x}", p3[y].get());
                        let p2 = p3.next_table_mut(y).unwrap();
                        for i in 0..511 {
                            if(p2[i].is_unused() == false) {
                                 println!("    entry: {:#x}", p2[i].get());
                                 let p1 = p2.next_table_mut(i).unwrap();
                                 for m in 0..511 {
                                     if(p1[m].is_unused() == false) {
                                         println!("      entry: {:#x}", p1[m].get());
                                     }
                                 }
                            }
                        }
                    }
                }
            }
        }
        
    }

    pub fn clone_page_table<A>(&mut self, allocator: &mut A ) 
        -> InactivePageTable
        where A: FrameAllocator
        {

        let mut temporary_page = TemporaryPage::new(Page { number: 0xcafebabe },
            allocator);

        let frame = allocator.allocate_frame().expect("no more frames");
        
        let mut new_table = {
            InactivePageTable::new(frame.clone(), self, &mut temporary_page)
        };
        let mut table_clone = self.clone();
        let p4 = table_clone.mapper.p4_mut();
            for x in 0..511 {
                if( p4[x].is_unused() == false ) {
                    let p3 = p4.next_table_mut(x).unwrap();
                    for y in 0..511 {
                        if(p3[y].is_unused() == false) {
                            let p2 = p3.next_table_mut(y).unwrap();
                            for i in 0..511 {
                                if(p2[i].is_unused() == false) {
                                    let p1 = p2.next_table_mut(i).unwrap();
                                    for m in 0..511 {
                                        if(p1[m].is_unused() == false) {
                                            let pflags = p1[m].flags();
                                            let mut frame =  p1[m].pointed_frame().unwrap();

                                            //crazy time 
                                            //let mut new_frame = allocator.allocate_frame().expect("out of memory");
                                            //frame.deep_copy(&mut new_frame);
                                            self.with(&mut new_table, &mut temporary_page, |mapper| {
                                                mapper.map_to_raw(x,y,i,m,frame, pflags, allocator);
                                            });
                                            //let np3 = new_p4_table.next_table_create(x, allocator);
                                            //let np2 = np3.next_table_create(y, allocator);
                                            //let np1 = np2.next_table_create(i, allocator);
                                            //np1[m].set(p1[m].pointed_frame().unwrap(), p1[m].flags());
                                            //println!("      entry: {:#x}", p1[m].get());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
        }
        
        new_table
        

    }

    pub fn with<F>(&mut self,
                   table: &mut InactivePageTable,
                   temporary_page: &mut temporary_page::TemporaryPage, // new
                   f: F)
    where F: FnOnce(&mut Mapper)
    {
        use x86_64::instructions::tlb;
        use x86_64::registers::control_regs;

        {
            let backup = Frame::containing_address(
                control_regs::cr3().0 as usize);

            // map temporary_page to current p4 table
            let p4_table = temporary_page.map_table_frame(backup.clone(), self);

            // overwrite recursive mapping
            self.p4_mut()[511].set(table.p4_frame.clone(), PRESENT | WRITABLE);
            tlb::flush_all();

            // execute f in the new context
            f(self);

            // restore recursive mapping to original p4 table
            p4_table[511].set(backup, PRESENT | WRITABLE);
            tlb::flush_all();
        }

        temporary_page.unmap(self);
    }
}

pub struct InactivePageTable {
    pub p4_frame: Frame,
}

impl InactivePageTable {
    pub fn new(frame: Frame,
               active_table: &mut ActivePageTable,
               temporary_page: &mut TemporaryPage)
               -> InactivePageTable {
        {
            let table = temporary_page.map_table_frame(frame.clone(),
                active_table);
            // now we are able to zero the table
            table.zero();
            // set up recursive mapping for the table
            table[511].set(frame.clone(), PRESENT | WRITABLE);
        }
        temporary_page.unmap(active_table);

        InactivePageTable { p4_frame: frame }
    }
}


pub fn remap_the_kernel<A>(allocator: &mut A, boot_info: &BootInformation)
    -> ActivePageTable
    where A: FrameAllocator
{
    let mut temporary_page = TemporaryPage::new(Page { number: 0xcafebabe },
        allocator);

    let mut active_table = unsafe { ActivePageTable::new() };
    let mut new_table = {
        let frame = allocator.allocate_frame().expect("no more frames");
        InactivePageTable::new(frame, &mut active_table, &mut temporary_page)
    };

    active_table.with(&mut new_table, &mut temporary_page, |mapper| {
        

        let elf_sections_tag = boot_info.elf_sections_tag()
            .expect("Memory map tag required");

        for section in elf_sections_tag.sections() {
            //use self::entry::WRITABLE;

            if !section.is_allocated() {
                // section is not loaded to memory
                continue;
            }
            assert!((section.start_address() as usize) % PAGE_SIZE == 0,
                    "sections need to be page aligned");

            println!("mapping section at addr: {:#x}, size: {:#x}",
                section.start_address(), section.size());

            let flags = EntryFlags::from_elf_section_flags(&section);

            let start_frame = Frame::containing_address(section.start_address() as usize);
            let end_frame = Frame::containing_address((section.end_address() - 1) as usize);
            for frame in Frame::range_inclusive(start_frame, end_frame) {
                mapper.identity_map(frame, flags, allocator);
            }
        }

        // identity map the VGA text buffer
        let vga_buffer_frame = Frame::containing_address(0xb8000);
        mapper.identity_map(vga_buffer_frame, WRITABLE, allocator);

        // identity map the multiboot info structure
        let multiboot_start = Frame::containing_address(boot_info.start_address() as usize);
        let multiboot_end = Frame::containing_address((boot_info.end_address() - 1) as usize);
        for frame in Frame::range_inclusive(multiboot_start, multiboot_end) {
            mapper.identity_map(frame, PRESENT, allocator);
        }

        // identiy map the initd space so our allocator doesnt write over it
        let modules = boot_info.module_tags();
        for module in modules {
            let module_start = Frame::containing_address(module.start_address() as usize);
            let module_end = Frame::containing_address((module.end_address() - 1) as usize);
            println!("Mapping Module {:#x}, {:#x}", module.start_address(), module.end_address() -1);
            for frame in Frame::range_inclusive(module_start, module_end) {
                mapper.identity_map(frame, PRESENT, allocator);
            }
        }

    
    });

    let old_table = active_table.switch(new_table);
    println!("NEW TABLE!!!");

    // turn the old p4 page into a guard page
    let old_p4_page = Page::containing_address(
      old_table.p4_frame.start_address()
    );
    active_table.unmap(old_p4_page, allocator);
    println!("guard page at {:#x}", old_p4_page.start_address());

    //active_table.test_walk();
   

    active_table
}


use core::ops::Add;

impl Add<usize> for Page {
    type Output = Page;

    fn add(self, rhs: usize) -> Page {
        Page { number: self.number + rhs }
    }
}