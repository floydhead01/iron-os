use drivers::vfs;
use drivers::vfs::node::{Node, DirEntry, FS_DIRECTORY, FS_FILE, VFSNode, VFSNodeInfo};
use alloc::Vec;
use alloc::boxed::Box;
use alloc::String;
use alloc::str;
use core::ptr::Unique;
use spin::Mutex;
use utils::{read_u32, read_u8};

pub struct RamBuffer {
    buffer: [u8; 4096]
}

#[derive(Clone)]
pub struct RamFSNode {
    pub buffer: Unique<RamBuffer>,
    pub name: Vec<u8>,
    pub mask: u32,
    pub uid: u32,
    pub gid: u32,
    pub flags: u32,
    pub inode: u32,
    pub length: u32,
    pub impldef: u32,  
    pub children: Vec<String>, 
    pub magic: u8,
    pub offset: u32,
}

impl VFSNode for RamFSNode {
    fn read (&mut self, offset: &u32, size: &u32, buffer: &mut Vec<u8>) -> i32{ 
        if offset + size > self.length
        {
            return -1;
        }

        let base_offset =  offset + self.offset;
        for x in 0..*size {
            unsafe {
                buffer.push(read_u8(&self.buffer.as_mut().buffer, base_offset + x));
            }
        }

        return 0;
    }

    fn write (&mut self, offset: &u32, size: &u32, buffer: &Vec<u8>) -> i32{
        return -1;
    }

    fn open(&mut self, read: bool, write: bool) {
        return;
    }
    fn close(&mut self) {
        return;
    }
    fn read_dir(&mut self) -> Vec<DirEntry> {
        let mut dirs: Vec<DirEntry> = vec![];
        let children: Vec<String> = self.children.clone().to_vec();
        for child in  children {
            match  vfs::VFS.lock().get_node(child) {
                Some(nodes) => {
                    let node_info = nodes.get_info();
                    dirs.push(DirEntry { name: node_info.name.clone(), inode: node_info.inode});
                },
                None => {}
            }
        }
        return dirs;
    }
    fn add_child(&mut self, child: String) {
        self.children.push(child);
    }
    fn get_info(&mut self) -> VFSNodeInfo {
        return VFSNodeInfo {
            name: self.name.clone(),
            mask: self.mask,
            uid: self.uid,
            gid: self.gid,
            flags: self.flags,
            inode: self.inode,
            length: self.length,
            impldef: self.impldef,
            children: self.children.clone() 
        }
    }
}


pub struct RamFS {
    //pub file_info: Vec<RamFS>,
    //pub buffer: Unique<RamBuffer>,
}

impl RamFS {
    pub fn init(&mut self, address: u32 ) {

        println!("loading initd ram disk");

        let m: Node = Node {
            name: String::from("initd").into_bytes(),
            mask: 0,
            uid: 0,
            gid: 0,
            flags: FS_DIRECTORY,
            inode: 0,
            length: 0,
            impldef: 0,
            children: vec![],
        };

        vfs::VFS.lock().add_node(String::from("/"), Box::new(m));

        unsafe{
            let mut buffer: Unique<RamBuffer> = unsafe { Unique::new_unchecked(address as *mut _) };
            let num_files = read_u32(&buffer.as_mut().buffer, 0);
            println!("Number of files in Init {}", num_files);
            for x in 0..num_files {
                let base_offset = 4 + (73 * x);
                let magic: u8 = read_u8(&buffer.as_mut().buffer, base_offset);
                let name: [u8; 64] = read_name(&buffer.as_mut().buffer, base_offset + 1);

                let offset: u32 = read_u32(&buffer.as_mut().buffer, base_offset + 65);
                let length: u32 = read_u32(&buffer.as_mut().buffer, base_offset + 69);
                let strname = String::from(String::from(str::from_utf8(&name).unwrap()).trim());
                println!("file: {}, magix: {}, offset: {}, length: {}", strname.clone(), magic, offset, length);

                let n = RamFSNode {
                    buffer: unsafe { Unique::new_unchecked(address as *mut _) },
                    name: strname.into_bytes(),
                    mask: 0,
                    uid: 0,
                    gid: 0,
                    flags: FS_FILE,
                    inode: 0,
                    length: length,
                    impldef: 0,
                    children: vec![],
                    magic: magic,
                    offset: offset,
                };

                vfs::VFS.lock().add_node(String::from("/initd"), Box::new(n));
            }
        }
    }
}

fn read_name(buffer: &[u8; 4096], start: u32) -> [u8; 64] {
    let mut temp: [u8; 64] = [0;64];
    for x in 0..64 {
        temp[x] = buffer[x + start as usize];
    }
    return temp;
}
