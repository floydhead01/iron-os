//virtual filesystem
use alloc::Vec;
use alloc::boxed::Box;
use alloc::String;
use super::VFS;

#[derive(Clone)]
pub struct VFSNodeInfo{
    pub name: Vec<u8>,
    pub mask: u32,
    pub uid: u32,
    pub gid: u32,
    pub flags: u32,
    pub inode: u32,
    pub length: u32,
    pub impldef: u32,  
    pub children: Vec<String>,  
}

pub trait VFSNode {
    fn read(&mut self, offset: &u32, size: &u32, buffer: &mut Vec<u8>) -> i32;
    fn write(&mut self, offset: &u32, size: &u32, buffer: &Vec<u8>) -> i32;
    fn open(&mut self, read: bool, write: bool);
    fn close(&mut self);
    fn read_dir(&mut self) -> Vec<DirEntry>;
    fn get_info(&mut self) -> VFSNodeInfo;
    fn add_child(&mut self, child: String);
}

#[derive(Clone)]
pub struct DirEntry {
    pub name: Vec<u8>,
    pub inode: u32,
}

pub const FS_FILE: u32 = 0x01;
pub const FS_DIRECTORY: u32 = 0x02;
pub const FS_CHARDEVICE: u32 = 0x03;
pub const FS_BLOCKDEVICE: u32 = 0x04;
pub const FS_PIPE: u32 = 0x05;
pub const FS_SYMLINK: u32 = 0x06;
pub const FS_MOUNTPOINT: u32 = 0x08;

pub struct Node {
    pub name: Vec<u8>,
    pub mask: u32,
    pub uid: u32,
    pub gid: u32,
    pub flags: u32,
    pub inode: u32,
    pub length: u32,
    pub impldef: u32,  
    pub children: Vec<String>, 
}

impl VFSNode for Node {
    fn read(&mut self, offset: &u32, size: &u32, buffer: &mut Vec<u8>) -> i32 {
        return -1;
    }
    fn write(&mut self, offset: &u32, size: &u32, buffer: &Vec<u8>) -> i32 {
        return -1;
    }
    fn open(&mut self, read: bool, write: bool) {
        return;
    }
    fn close(&mut self) {
        return;
    }
    fn get_info(&mut self) -> VFSNodeInfo {
        return VFSNodeInfo {
            name: self.name.clone(),
            mask: self.mask,
            uid: self.uid,
            gid: self.gid,
            flags: self.flags,
            inode: self.inode,
            length: self.length,
            impldef: self.impldef,
            children: self.children.clone() 
        }
    }
    fn read_dir(&mut self) -> Vec<DirEntry> {
        let mut dirs: Vec<DirEntry> = vec![];
        let children: Vec<String> = self.children.clone().to_vec();
        for child in  children {
            dirs.push(DirEntry { name: child.into_bytes(), inode: 0});
            /*
            match  VFS.lock().get_node(child) {
                Some(nodes) => {
                    let node_info = nodes.get_info();
                    dirs.push(DirEntry { name: node_info.name.clone(), inode 0});
                },
                None => {}
            }*/
        }
        return dirs;
    }

    fn add_child(&mut self, child: String) {
        self.children.push(child);
    }
}