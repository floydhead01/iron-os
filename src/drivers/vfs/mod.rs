pub mod node;
use self::node::{Node, DirEntry, FS_DIRECTORY, VFSNode, VFSNodeInfo};
use alloc::BTreeMap;
use alloc::Vec;
use alloc::boxed::Box;
use alloc::String;
use spin::Mutex;

lazy_static!{
    pub static ref VFS: Mutex<VirtualFileSystem> = Mutex::new(VirtualFileSystem { nodes: BTreeMap::new()});
}

pub struct VirtualFileSystem {
    pub nodes: BTreeMap<String, Box<VFSNode + Send>>,
}

impl VirtualFileSystem {

    pub fn init(&mut self) {
        let mut root_node = Node { 
            name: String::from("/").into_bytes(),
            mask: 0,
            uid: 0,
            gid: 0,
            flags: FS_DIRECTORY,
            inode: 0,
            length: 0,
            impldef: 0,
            children: vec![]
        };

        self.nodes.insert(String::from(""), Box::new(root_node));
    }

    pub fn add_node(&mut self, mut parent_path: String, mut node: Box<VFSNode + Send>) -> i32 {
        use alloc::str;
        if parent_path == "/" {
            parent_path = String::from("");
        }

        let node_info = node.get_info(); 

        let full_path = String::from(parent_path.clone() + "/" + &*String::from_utf8(node_info.name).unwrap());

        //println!("adding: {}", &*full_path.clone() );
        {
            let mut op = self.get_node(parent_path.clone()).unwrap();
            op.add_child(String::from(full_path.clone()));
            //self.nodes.insert(parent_path.clone() , op);

        }

        self.nodes.insert(full_path.clone(), node);
        
        return 1;
    }
    pub fn remove_node(&mut self, path: String) -> i32 {
        return 0;
    }
    pub fn get_node(&mut self, path: String) -> Option<&mut Box<VFSNode + Send>> {
        return self.nodes.get_mut(&path);
    }
    pub fn update_node(&mut self) -> i32 {
        return 0;
    }
    
}




