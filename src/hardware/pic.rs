use x86_64::instructions::port::*;

const PIC1_CMD_PORT: u16 =  0x20;
const PIC2_CMD_PORT: u16 = 0xA0;
const PIC1_DATA_PORT: u16  = 0x21;
const PIC2_DATA_PORT: u16  = 0xA1;

const PIC1: Pic = Pic::new(PIC1_CMD_PORT, PIC1_DATA_PORT);
const PIC2: Pic = Pic::new(PIC2_CMD_PORT, PIC2_DATA_PORT);

pub struct Pic {
    command_port: u16,
    data_port: u16
}

/**
 * Pic Controller 
 */
impl Pic {

	pub const fn new(command_port: u16, data_port: u16) -> Self {
        Pic { command_port: command_port, data_port: data_port }
    }

	pub fn init(&self, offset: u8, is_master: bool) {
		unsafe {
			outb(self.command_port, 0x11);
			wait();
			outb(self.data_port, offset);
			wait();
			if is_master { outb(self.data_port, 0x04); } else { outb(self.data_port, 0x02); }
			wait();
			outb(self.data_port, 0x01);
			wait();
			outb(self.data_port, 0xFF);
		}
	}
	pub fn set_mask(&self, mask: u8) {
		unsafe{  outb(self.data_port, mask); }
	}

	fn enable_irq(&self, irqline: u8) {
		unsafe {
			let value= inb(self.data_port) | (1 << irqline);
    		self.set_mask(value);     
		}
	}
	fn disable_irq(&self, irqline: u8) {
		unsafe {
			let value = inb(self.data_port)  & !(1 << irqline);
    		self.set_mask(value);      
		}
	}
	pub fn set_irq(&self, irqline: u8, enabled: bool) {
		if enabled {
			self.disable_irq(irqline);
		} else {
			self.enable_irq(irqline);
		}
	}
	pub fn resume(&self) {
		unsafe {  outb(self.command_port, 0x20); }
	}
}

pub fn init(){

	PIC1.init(0x20, true);
	println!("PIC:1, State: Master, offset: 0x20, INIT ");
	PIC2.init(0x28, false);
	println!("PIC:2, State: Slave, offset: 0x28, INIT ");
}

pub fn set_irq(mut irqline: u8, enabled: bool) {
	if irqline > 8 {
		irqline = irqline % 8;
		PIC2.set_irq(irqline, enabled);
	} else {
		PIC1.set_irq(irqline, enabled);
	}
}

pub fn enable_hardware(){
	unsafe{ asm!("sti"); }
	println!("Hardware Interupts Enabled ");
}
pub fn disable_hardware(){
	unsafe { asm!("cli"); }
	println!("Hardware Interupts Disabled ");
}

pub fn resume() {
	PIC1.resume();
	PIC2.resume();
}

fn wait(){
    unsafe{  outb(0x20 , 0);}
}