/**
 * Generic Helpers
 */
pub fn read_u8(buffer: &[u8; 4096], start: u32) -> u8 {
    return buffer[start as usize];
}

pub fn read_u32(buffer: &[u8; 4096], start: u32) -> u32 {
    let mut temp: [u8; 4] = [0;4];
    for x in 0..4 {
        temp[x] = buffer[x + start as usize];
    }
    return as_u32_be(&temp);
}

pub fn as_u32_be(array: &[u8; 4]) -> u32 {
    ((array[0] as u32) << 24) +
    ((array[1] as u32) << 16) +
    ((array[2] as u32) <<  8) +
    ((array[3] as u32) <<  0)
}

pub fn as_u32_le(array: &[u8; 4]) -> u32 {
    ((array[0] as u32) <<  0) +
    ((array[1] as u32) <<  8) +
    ((array[2] as u32) << 16) +
    ((array[3] as u32) << 24)
}