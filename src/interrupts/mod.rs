use x86_64::structures::idt::{Idt, ExceptionStackFrame};
//use x86_64::instructions::port::*;
use x86_64::structures::tss::TaskStateSegment;
use x86_64::VirtualAddress;
use x86_64::structures::gdt::SegmentSelector;
use x86_64::instructions::segmentation::set_cs;
use x86_64::instructions::tables::load_tss;

use spin::Once;

mod gdt;

use memory::MemoryController;


lazy_static! {
    pub static ref TSS: Once<TaskStateSegment> = Once::new();
}
pub static GDT: Once<gdt::Gdt> = Once::new();

lazy_static! {
    static ref IDT: Idt = {
        let mut idt = Idt::new();

        idt.breakpoint.set_handler_fn(breakpoint_handler);
        idt.divide_by_zero.set_handler_fn(divide_handler);
        unsafe {
            idt.double_fault.set_handler_fn(double_fault_handler)
                .set_stack_index(DOUBLE_FAULT_IST_INDEX as u16);
        }
        idt.debug.set_handler_fn(debug_handler);
        idt.non_maskable_interrupt.set_handler_fn(interrupt_handler);
        idt.overflow.set_handler_fn(overflow_handler);
        idt.bound_range_exceeded.set_handler_fn(bound_range_exceeded_handler);
        idt.invalid_opcode.set_handler_fn(invalid_opcode_handler);
        idt.invalid_tss.set_handler_fn(invalid_tss_handler);
        idt.segment_not_present.set_handler_fn(segment_not_present_handler);
        idt.interrupts[1].set_handler_fn(keyboard_handler);
        //idt.interrupts[33].set_handler_fn(keyboard_handler);
        idt.stack_segment_fault.set_handler_fn(seg_fault_handler);

        idt

    };
}

const DOUBLE_FAULT_IST_INDEX: usize = 0;

pub fn init(memory_controller: &mut MemoryController) {
    let double_fault_stack = memory_controller.alloc_stack(1)
        .expect("could not allocate double fault stack");
    let kernel_stack = memory_controller.alloc_stack(1)
        .expect("could not allocate double fault stack");

    let tss = TSS.call_once(|| {
        let mut tss = TaskStateSegment::new();
        tss.privilege_stack_table[0] = VirtualAddress ( kernel_stack.top() );
        tss.interrupt_stack_table[DOUBLE_FAULT_IST_INDEX] = VirtualAddress(
            double_fault_stack.top());
        tss
    });

   let mut code_selector = SegmentSelector(0);
    let mut tss_selector = SegmentSelector(0);
    let gdt = GDT.call_once(|| {
        let mut gdt = gdt::Gdt::new();
        code_selector = gdt.add_entry(gdt::Descriptor::kernel_code_segment());
        tss_selector = gdt.add_entry(gdt::Descriptor::tss_segment(&tss));
        gdt
    });
    gdt.load();

    unsafe {
        // reload code segment register
        set_cs(code_selector);
        // load TSS
        load_tss(tss_selector);
    }

    //init_pic();
    IDT.load();
    //enable_hardware();
}



extern "x86-interrupt" fn breakpoint_handler(
    stack_frame: &mut ExceptionStackFrame)
{
    println!("EXCEPTION: BREAKPOINT\n{:#?}", stack_frame);
}
extern "x86-interrupt" fn divide_handler(
    stack_frame: &mut ExceptionStackFrame)
{
    println!("EXCEPTION: DIVIDE BY ZERO \n{:#?}", stack_frame);
}

extern "x86-interrupt" fn double_fault_handler(
    stack_frame: &mut ExceptionStackFrame, _error_code: u64)
{
    println!("\nEXCEPTION: DOUBLE FAULT\n{:#?}", stack_frame);
    loop{};
}

extern "x86-interrupt" fn debug_handler(
    stack_frame: &mut ExceptionStackFrame)
{
    println!("\nEXCEPTION: Debug\n{:#?}", stack_frame);
}
extern "x86-interrupt" fn interrupt_handler(
    stack_frame: &mut ExceptionStackFrame)
{
    println!("\nEXCEPTION: Interrupt\n{:#?}", stack_frame);
}
extern "x86-interrupt" fn overflow_handler(
    stack_frame: &mut ExceptionStackFrame)
{
    println!("\nEXCEPTION: Overflow \n{:#?}", stack_frame);
}
extern "x86-interrupt" fn bound_range_exceeded_handler(
    stack_frame: &mut ExceptionStackFrame)
{
    println!("\nEXCEPTION: Bound Range Exceeded\n{:#?}", stack_frame);
}

extern "x86-interrupt" fn invalid_opcode_handler(
    stack_frame: &mut ExceptionStackFrame)
{
    println!("\nEXCEPTION: Invalid OpCode\n{:#?}", stack_frame);
}
extern "x86-interrupt" fn invalid_tss_handler(
    stack_frame: &mut ExceptionStackFrame, _error_code: u64)
{
    println!("\nEXCEPTION: Invalid TSS\n{:#?}", stack_frame);
}
extern "x86-interrupt" fn segment_not_present_handler(
    stack_frame: &mut ExceptionStackFrame, _error_code: u64)
{
    println!("\nEXCEPTION: Seg Not Present\n{:#?}", stack_frame);
}

extern "x86-interrupt" fn seg_fault_handler(
    stack_frame: &mut ExceptionStackFrame, _error_code: u64)
{
    println!("\nEXCEPTION: Seg Fault\n{:#?}", stack_frame);
}

extern "x86-interrupt" fn keyboard_handler(
    stack_frame: &mut ExceptionStackFrame)
{
    use drivers::keyboard::*;
    use hardware::pic::*;
    if let Some(c) = get_c() {
        print!("{}", c);
    }
    resume();
    
}